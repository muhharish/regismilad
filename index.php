<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" media="screen" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MILAD 17th KDCW</title>
    <script type="text/javascript">
    $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
    })
    </script>
    <script>
    function proses(){
        var regi=document.getElementById("ank").value;
        document.getElementById("reg").value=regi;
      }
    </script>
    <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="js/tether.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
  </head>
<body>

  <section class="atas">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12">
          <a class="hero-brand" href="index.html" title="Home"><img alt="" src="img/logo.png"></a>
        </div>
      </div>

      <div class="col-md-12">
        <h1>
          Anniversary 17th KeDai Computerworks
          </h1>
        <p class="tagline">
          "Sweet Memories 17th KeDai Computerworks"
        </p>
        <button type="button" class="but" data-toggle="modal" data-target
        =".bd-example-modal-lg">
          Register Now
        </button>
      </div>
    </div>
  </section>

  <!--modalnya-->
<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <br><br>
  <form action="registrasi.php" method="post">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <p style="text-align:center;font-size:20px; padding-top:0px;">Anniversary 17th<br><b>KeDai Computerworks</b></p>
      <div class="modal-body">
        <fieldset>
        <div class="row">
          <div class="form-group col-md-6" id="ang">
            <div>
            <select class="form-control" name="ank" id="ank" onchange="proses()">
              <option value='0' disabled="disabled" selected/>Pilih Angkatan</option>
              <option value=".KD.I.02">Angkatan I</option>
              <option value=".KD.II.03">Angkatan II</option>
              <option value=".KD.III.04">Angkatan III</option>
              <option value=".KD.IV.05">Angkatan IV</option>
              <option value=".KD.V.06">Angkatan V</option>
              <option value=".KD.VI.07">Angkatan VI</option>
              <option value=".KD.VII.08">Angkatan VII</option>
              <option value=".KD.VIII.09">Angkatan VIII</option>
              <option value=".KD.IX.10">Angkatan IX</option>
              <option value=".KD.X.11">Angkatan X</option>
              <option value=".KD.XI.12">Angkatan XI</option>
              <option value=".KD.XII.13">Angkatan XII</option>
              <option value=".KD.XIII.14">Angkatan XIII</option>
              <option value=".KD.XIV.15">Angkatan XIV</option>
              <option value=".KD.XV.16">Angkatan XV</option>
              <option value=".KD.XV.17">Angkatan XVI</option>
            </select>
            </div>
          </div>
              <div class="col-md-6  form-group">
                <div>
                  <input type="name" class="form-control" id="reg" name="noreg" placeholder="Masukan No registrasi">
                </div>
              </div>
          </div>
            <div>
              <div class="form-group">
                <div>
                  <input type="name" class="form-control" id="pwd" name="nama" placeholder="Masukan Nama">
                </div>
              </div>
              <div class="form-group">
                <div>
                  <input type="name" class="form-control" id="pwd" name="alamat" placeholder="Masukan Alamat">
                </div>
              </div>
              <div class="form-group">
                <div>
                  <input type="number" class="form-control" id="pwd" name="hp" placeholder="Masukan No.Hp ">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-6">
                  <div class="">
                  <select class="form-control" name="jkel">
                    <option value='0' disabled="disabled" selected/>Pilih Jenis Kelamin</option>
                    <option value="Laki-Laki">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                  </div>
                </div>
                <div class="col-6">
                  <div>
                    <select class="form-control" name="goldarah">
                      <option value='0' disabled="disabled" selected/>Pilih Golongan Darah</option>
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="O">O</option>
                      <option value="AB">AB</option>
                    </select>
                   </div>
                </div>
              </div>
              <div class="form-group">
                <div>
                  <input type="email" class="form-control" id="pwd" name="email" placeholder="Masukan Email">
                </div>
              </div>
              <div class="form-group">
                <div>
                  <input type="name" class="form-control" id="pwd" name="instansi" placeholder="Masukan Instansi Tempat Bekerja ">
                </div>
              </div>
          </diV> <!--modalvody-->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary" value="register">Simpan</button>
      </div>
    </div>
  </fieldset>
  </div>
</form>
</div>
<!--endmodal-->
</body>
</html>
